<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>litify_pm__Amount_Due_to_Client__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Amount Due to Client</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Attorney_Credited__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Attorney Credited</label>
        <referenceTo>User</referenceTo>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Contingency_Fee_Rate__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contingency Fee Rate (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>litify_pm__Days_to_Resolve__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(
AND(
NOT(ISBLANK(litify_pm__Matter__c)),
NOT(ISBLANK(litify_pm__Matter__r.litify_pm__Open_Date__c)),
NOT(ISBLANK(litify_pm__Resolution_Date__c))),

litify_pm__Resolution_Date__c-litify_pm__Matter__r.litify_pm__Open_Date__c, null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Days to Resolve</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Fees_Due_to_Others__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fees Due to Others</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Gross_Attorney_Fee__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Gross Attorney Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Insurance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Insurance</label>
        <referenceTo>litify_pm__Insurance__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Matter__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Matter</label>
        <referenceTo>litify_pm__Matter__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Negotiation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Negotiation</label>
        <referenceTo>litify_pm__Negotiation__c</referenceTo>
        <relationshipLabel>Resolutions</relationshipLabel>
        <relationshipName>LitifyResolutions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Net_Attorney_Fee__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Net Attorney Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Payor_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payor Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Attorney</fullName>
                    <default>false</default>
                    <label>Attorney</label>
                </value>
                <value>
                    <fullName>Defendant</fullName>
                    <default>false</default>
                    <label>Defendant</label>
                </value>
                <value>
                    <fullName>Insurance</fullName>
                    <default>false</default>
                    <label>Insurance</label>
                </value>
                <value>
                    <fullName>Third-Party Administrator (TPA)</fullName>
                    <default>false</default>
                    <label>Third-Party Administrator (TPA)</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>litify_pm__Payor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payor</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Resolutions (Payor)</relationshipLabel>
        <relationshipName>LitifyResolutionsPayor</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Reason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reason</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>litify_pm__Resolution_Type__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Calls not being returned</fullName>
                    <default>false</default>
                    <label>Calls not being returned</label>
                </value>
                <value>
                    <fullName>Cannot locate defendant</fullName>
                    <default>false</default>
                    <label>Cannot locate defendant</label>
                </value>
                <value>
                    <fullName>Client did not treat</fullName>
                    <default>false</default>
                    <label>Client did not treat</label>
                </value>
                <value>
                    <fullName>Client missing</fullName>
                    <default>false</default>
                    <label>Client missing</label>
                </value>
                <value>
                    <fullName>Client Paid Correctly</fullName>
                    <default>false</default>
                    <label>Client Paid Correctly</label>
                </value>
                <value>
                    <fullName>Client unavailable or deceased</fullName>
                    <default>false</default>
                    <label>Client unavailable or deceased</label>
                </value>
                <value>
                    <fullName>Conflict of Interest</fullName>
                    <default>false</default>
                    <label>Conflict of Interest</label>
                </value>
                <value>
                    <fullName>Decided not to pursue claim</fullName>
                    <default>false</default>
                    <label>Decided not to pursue claim</label>
                </value>
                <value>
                    <fullName>Decided to accept prior offer</fullName>
                    <default>false</default>
                    <label>Decided to accept prior offer</label>
                </value>
                <value>
                    <fullName>Defendant did not respond</fullName>
                    <default>false</default>
                    <label>Defendant did not respond</label>
                </value>
                <value>
                    <fullName>Did not wish to treat</fullName>
                    <default>false</default>
                    <label>Did not wish to treat</label>
                </value>
                <value>
                    <fullName>Dissatisfied with handling of case</fullName>
                    <default>false</default>
                    <label>Dissatisfied with handling of case</label>
                </value>
                <value>
                    <fullName>Employees not polite</fullName>
                    <default>false</default>
                    <label>Employees not polite</label>
                </value>
                <value>
                    <fullName>Firm Withdrew from case</fullName>
                    <default>false</default>
                    <label>Firm Withdrew from case</label>
                </value>
                <value>
                    <fullName>Injuries too minor or unrelated</fullName>
                    <default>false</default>
                    <label>Injuries too minor or unrelated</label>
                </value>
                <value>
                    <fullName>Liability issues</fullName>
                    <default>false</default>
                    <label>Liability issues</label>
                </value>
                <value>
                    <fullName>Low offer</fullName>
                    <default>false</default>
                    <label>Low offer</label>
                </value>
                <value>
                    <fullName>Negative review of medical records</fullName>
                    <default>false</default>
                    <label>Negative review of medical records</label>
                </value>
                <value>
                    <fullName>No insurance or assets</fullName>
                    <default>false</default>
                    <label>No insurance or assets</label>
                </value>
                <value>
                    <fullName>Not in Ref Atty County (PIP cases)</fullName>
                    <default>false</default>
                    <label>Not in Ref Atty County (PIP cases)</label>
                </value>
                <value>
                    <fullName>Not notified of change of assistant</fullName>
                    <default>false</default>
                    <label>Not notified of change of assistant</label>
                </value>
                <value>
                    <fullName>Offer too low</fullName>
                    <default>false</default>
                    <label>Offer too low</label>
                </value>
                <value>
                    <fullName>PIP benefits exhausted (PIP cases)</fullName>
                    <default>false</default>
                    <label>PIP benefits exhausted (PIP cases)</label>
                </value>
                <value>
                    <fullName>PIP claim already filed (PIP cases)</fullName>
                    <default>false</default>
                    <label>PIP claim already filed (PIP cases)</label>
                </value>
                <value>
                    <fullName>Property damage not resolved</fullName>
                    <default>false</default>
                    <label>Property damage not resolved</label>
                </value>
                <value>
                    <fullName>Reasons unrelated to firm/case</fullName>
                    <default>false</default>
                    <label>Reasons unrelated to firm/case</label>
                </value>
                <value>
                    <fullName>Too difficult to speak to attorney</fullName>
                    <default>false</default>
                    <label>Too difficult to speak to attorney</label>
                </value>
                <value>
                    <fullName>Uncooperative client</fullName>
                    <default>false</default>
                    <label>Uncooperative client</label>
                </value>
                <value>
                    <fullName>Upset that case is being referred.</fullName>
                    <default>false</default>
                    <label>Upset that case is being referred.</label>
                </value>
                <value>
                    <fullName>Unknown</fullName>
                    <default>false</default>
                    <label>Unknown</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Cannot locate defendant</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Client missing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Client Paid Correctly</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Client unavailable or deceased</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Conflict of Interest</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Injuries too minor or unrelated</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Liability issues</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Low offer</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Negative review of medical records</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>No insurance or assets</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Not in Ref Atty County (PIP cases)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Discharge</controllingFieldValue>
                <valueName>Uncooperative client</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Calls not being returned</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Decided not to pursue claim</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Decided to accept prior offer</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Did not wish to treat</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Dissatisfied with handling of case</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Employees not polite</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Not notified of change of assistant</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Offer too low</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Property damage not resolved</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Reasons unrelated to firm/case</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Too difficult to speak to attorney</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Upset that case is being referred.</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Fired</controllingFieldValue>
                <valueName>Unknown</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>litify_pm__Resolution_Date__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resolution Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>litify_pm__Resolution_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resolution Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Arbitration</fullName>
                    <default>false</default>
                    <label>Arbitration</label>
                </value>
                <value>
                    <fullName>Defendant Verdict – No Appeal</fullName>
                    <default>false</default>
                    <label>Defendant Verdict – No Appeal</label>
                </value>
                <value>
                    <fullName>Directed Verdict for Defendant</fullName>
                    <default>false</default>
                    <label>Directed Verdict for Defendant</label>
                </value>
                <value>
                    <fullName>Discharge</fullName>
                    <default>false</default>
                    <label>Discharge</label>
                </value>
                <value>
                    <fullName>Dismissed for Fraud</fullName>
                    <default>false</default>
                    <label>Dismissed for Fraud</label>
                </value>
                <value>
                    <fullName>Fired</fullName>
                    <default>false</default>
                    <label>Fired</label>
                </value>
                <value>
                    <fullName>Jury Verdict</fullName>
                    <default>false</default>
                    <label>Jury Verdict</label>
                </value>
                <value>
                    <fullName>Lost Appeal</fullName>
                    <default>false</default>
                    <label>Lost Appeal</label>
                </value>
                <value>
                    <fullName>Low Verdict – Client Owes D’s Fees/Costs</fullName>
                    <default>false</default>
                    <label>Low Verdict – Client Owes D’s Fees/Costs</label>
                </value>
                <value>
                    <fullName>Mediation</fullName>
                    <default>false</default>
                    <label>Mediation</label>
                </value>
                <value>
                    <fullName>MSJ Appeal Lost</fullName>
                    <default>false</default>
                    <label>MSJ Appeal Lost</label>
                </value>
                <value>
                    <fullName>MSJ Lost – No Appeal</fullName>
                    <default>false</default>
                    <label>MSJ Lost – No Appeal</label>
                </value>
                <value>
                    <fullName>Negotiation - Filed Suit</fullName>
                    <default>false</default>
                    <label>Negotiation - Filed Suit</label>
                </value>
                <value>
                    <fullName>Negotiation - Not Filed</fullName>
                    <default>false</default>
                    <label>Negotiation - Not Filed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>litify_pm__Resolved_by__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Resolved by</label>
        <referenceTo>User</referenceTo>
        <relationshipName>LitifyResolutionsResolvedBy</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>litify_pm__Settlement_Verdict_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Settlement/Verdict Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Total_Damages__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Damages</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>litify_pm__Total_Expenses__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Expenses</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Resolution</label>
    <listViews>
        <fullName>litify_pm__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>RES-{00000}</displayFormat>
        <label>Resolution Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Resolutions</pluralLabel>
    <recordTypes>
        <fullName>litify_pm__Resolution</fullName>
        <active>true</active>
        <label>Resolution</label>
        <picklistValues>
            <picklist>litify_pm__Payor_Type__c</picklist>
            <values>
                <fullName>Attorney</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Defendant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Insurance</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Third-Party Administrator %28TPA%29</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>litify_pm__Reason__c</picklist>
            <values>
                <fullName>Calls not being returned</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Cannot locate defendant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client Paid Correctly</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client did not treat</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client missing</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Client unavailable or deceased</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Conflict of Interest</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Decided not to pursue claim</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Decided to accept prior offer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Defendant did not respond</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Did not wish to treat</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dissatisfied with handling of case</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Employees not polite</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Firm Withdrew from case</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Injuries too minor or unrelated</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Liability issues</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low offer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negative review of medical records</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>No insurance or assets</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not in Ref Atty County %28PIP cases%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not notified of change of assistant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Offer too low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>PIP benefits exhausted %28PIP cases%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>PIP claim already filed %28PIP cases%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Property damage not resolved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Reasons unrelated to firm%2Fcase</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Too difficult to speak to attorney</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Uncooperative client</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Unknown</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Upset that case is being referred%2E</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>litify_pm__Resolution_Type__c</picklist>
            <values>
                <fullName>Arbitration</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Defendant Verdict %E2%80%93 No Appeal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Directed Verdict for Defendant</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Discharge</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dismissed for Fraud</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Fired</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Jury Verdict</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lost Appeal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low Verdict %E2%80%93 Client Owes D%E2%80%99s Fees%2FCosts</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>MSJ Appeal Lost</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>MSJ Lost %E2%80%93 No Appeal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mediation</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negotiation - Filed Suit</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Negotiation - Not Filed</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
