<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>litify_pm__Auto_Add_Task__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>if it is true when a stage on a matter is set as active we create task automatically for this stage</description>
        <externalId>false</externalId>
        <label>Auto Add Task</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__Auto_Add_Triggered_Task__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>when it is true if we are in a matter stage and there are some task that can&apos;t be created for example because due date depend on some date field on matter, when we fill this date field those task are going to be created automatically.</description>
        <externalId>false</externalId>
        <label>Auto Add Triggered Task</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__Billable_Interval__c</fullName>
        <defaultValue>6</defaultValue>
        <deprecated>false</deprecated>
        <description>This interval is going to be the one used to calculate the billable time when recording time in time entries</description>
        <externalId>false</externalId>
        <inlineHelpText>This interval is going to be the one used to calculate the billable time when recording time in time entries. It is expressed in minutes.</inlineHelpText>
        <label>Billable Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__DICE_Navigate_To__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>DICE Navigate To</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__DisableEmailMatchingRuleOnIntakesAPI__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This setting allows to disable email matching rule on Intakes API</description>
        <externalId>false</externalId>
        <inlineHelpText>Allows disabling of email matching rule for new intakes from the Intake API</inlineHelpText>
        <label>DisableEmailMatchingRuleOnIntakesAPI</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__DisableEmailMatchingRuleOnReferralIntake__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Disables the email matching rule for parties created during referral to intake conversion.</description>
        <externalId>false</externalId>
        <inlineHelpText>Disables merging of parties that share the same email address when creating an intake from a referral.</inlineHelpText>
        <label>DisableEmailMatchingRuleOnReferralIntake</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__Disable_Account_1to1_Enforce__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This setup allows to disable the 1:1 enforcement on account that forces account to have 1 and only 1 contact.</description>
        <externalId>false</externalId>
        <label>Disable Account 1 to 1 Enforce</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__Referral_API_endpoint__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Referral API endpoint</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>litify_pm__Run_Triggers__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Custom setting that controls the global execution of trigger execution.  To globally disable triggers, uncheck this box.</description>
        <externalId>false</externalId>
        <inlineHelpText>Custom setting that controls the global execution of trigger execution.  To globally disable triggers, uncheck this box.</inlineHelpText>
        <label>Run Triggers</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__Scheduler_Auto_Delete_Logs_Interval__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Scheduler Auto Delete Logs Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Scheduler_Case_Type_Interval__c</fullName>
        <deprecated>false</deprecated>
        <description>CaseType interval in seconds</description>
        <externalId>false</externalId>
        <label>Scheduler Case Type Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Scheduler_Firm_Interval__c</fullName>
        <deprecated>false</deprecated>
        <description>Firm interval in seconds</description>
        <externalId>false</externalId>
        <label>Scheduler Firm Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Scheduler_Incoming_Interval__c</fullName>
        <deprecated>false</deprecated>
        <description>Incoming Referrals interval in seconds</description>
        <externalId>false</externalId>
        <label>Scheduler Incoming Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Scheduler_Outgoing_Interval__c</fullName>
        <deprecated>false</deprecated>
        <description>Outgoing Referrals interval in seconds</description>
        <externalId>false</externalId>
        <label>Scheduler Outgoing Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Scheduler_Post_Interval__c</fullName>
        <deprecated>false</deprecated>
        <description>Send Referrals interval in seconds</description>
        <externalId>false</externalId>
        <label>Scheduler Post Interval</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>litify_pm__Use_long_text_for_Time_Entry_description__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked we will use Time_Entry_Description_Long__c for time entries, if unchecked we will use Time_Entry_Description__c</description>
        <externalId>false</externalId>
        <label>Use long text for Time Entry description</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>litify_pm__litify_organization_id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>litify organization id</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Public Setup</label>
    <visibility>Public</visibility>
</CustomObject>
