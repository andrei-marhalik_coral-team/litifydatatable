({
    doInit : function(cmp, event, helper) {
        console.log("init started")
        cmp.set('v.atomicChanges', []);
        cmp.set('v.draftValues', []);
        var actions = [
            { label: 'Edit', name: 'edit_account' },
            { label: 'Delete', name: 'delete' },
            { label: 'Change Owner', name: 'change_owner' },
        ];
            cmp.set('v.mycolumns', [
            { label: 'Contact Name', fieldName: 'contactUrl', type: 'url', initialWidth: 175, sortable :true, typeAttributes: { label: { fieldName: 'Name' } } }, 
            { label: 'Birthdate', fieldName: 'Birthdate', type: 'date', initialWidth: 120, editable: true, sortable :true },
            { label: 'Phone', fieldName: 'Phone', type: 'phone', initialWidth: 150, editable: true, sortable :true},
            { label: 'Email', fieldName: 'Email', type: 'email', initialWidth: 170, editable: true, sortable :true },
            { label: 'Account', fieldName: 'accounturl', type: 'url', initialWidth: 130, sortable :true, typeAttributes: { label: { fieldName: 'AccountId' } } },
            { label: 'Gender', fieldName: 'litify_pm__Gender__c',  initialWidth: 80, type: 'button', cellAttributes: 
            					{ alignment: 'center' }, typeAttributes: { label: {fieldName: 'litify_pm__Gender__c'}, title: 'Edit Gender', name: 'edit_account', variant: 'base'}},
            { label: 'Edit', initialWidth: 80, type: 'button', cellAttributes: { alignment: 'center' }, typeAttributes: {title: 'Edit Row', name: 'edit_account', iconName: 'utility:edit'}}
        ]);
        
        helper.handleGetData(cmp);
        helper.resolveSaveLocalStorage(cmp);
        helper.resolveAutoSaveValue(cmp);
        if (cmp.get('v.saveLocalStorage')) {
            helper.resolveDraftValues(cmp);
        }   
    },
     
    // the lookup icon is a button, which uses onrowaction
    // include other row actions here - I removed to keep this code snippet shorter 
    onRowAction : function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        var rows = cmp.get('v.mydata');
        cmp.set("v.selectedRow", row.Id);
        switch (action.name) {
            case 'edit_account':
                var rowId = row.Id;
                var rowIndex = rows.indexOf(row);
                cmp.set('v.rowinfo', rowIndex);
                helper.handleEditModal(cmp, rowId, helper);
                break;
           
            case 'delete':
                //delete code here
                break;
        }
    },
    
     onLookupUpdate : function(cmp, event, helper) {
        var rows =  cmp.get('v.mydata');
        var indexMap =[];
        if(rows){
        rows.forEach(function (row) {
             indexMap.push ({"index" : rows.indexOf(row), "key": row.Id});
            });
        localStorage.setItem('indexMap-save-local', JSON.stringify(indexMap));
        }
        helper.handleGetData(cmp, event, helper);
    },
    
    onSave: function(cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        helper.saveChanges(cmp, draftValues);
    },
    
    onSaveInLocalStorage : function(cmp, event, helper) {
        helper.handleSaveInLocalStorage(cmp, event);
    }, 
    
    onEditCell : function(cmp, event, helper) {
		helper.handleEditCell(cmp, event);
	},
    
    onCancel : function(cmp, event, helper) {
        helper.clearDraftValuesLS();
    },    
    
    onHeaderAction: function (cmp, event, helper) {
        var actionName = event.getParam('action').name;
        var colDef = event.getParam('columnDefinition');
        var columns = cmp.get('v.mycolumns');
        console.log('columns-->' + columns);
        var activeFilter = cmp.get('v.activeFilter');
        if (actionName !== activeFilter) {
            var idx = columns.indexOf(colDef);
            var actions = columns[idx].actions;
            actions.forEach(function (action) {
                action.checked = action.name === actionName;
            });
            cmp.set('v.activeFilter', actionName);
            cmp.set('v.mycolumns', columns);
        }
    },
    
    onColumnSorting : function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        event.getSource().set("v.sortedBy", fieldName);
        event.getSource().set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    
    onClick : function(cmp, event, helper) {
        cmp.set("v.typeTest", "date");
    },
   
})