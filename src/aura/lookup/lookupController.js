({
    doInit : function (cmp, event, helper) {
        var str = cmp.get('v.fieldsToDisplay').replace(/\s+/g,'');
        var array = str.split(",");
        cmp.set('v.fieldsArray', array)    
    },
   
    showToast : function (cmp, event, helper){
       var appEvent = $A.get('e.c:evt_dataTableUpdate');
        appEvent.setParams({
            "refreshMe" : "we dont need a param.. must lookup if it's practice to use one"    
        });
        appEvent.fire();	
            cmp.find('notifLib').showToast({
            variant: 'success',
            message: "The account has been updated."
             
        });
        cmp.set("v.isOpen", false);
        cmp.find("popuplib").notifyClose();
    },
   closeModal: function(cmp, event, helper) {
       // set "isOpen" attribute to false for hide/close model box 
       cmp.set("v.isOpen", false);
       cmp.find("popuplib").notifyClose();
    },
    
})