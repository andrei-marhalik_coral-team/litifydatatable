public class ContentNoteMediator {
    public final static SObjectField TITLE_SOBJECTFIELD = Schema.ContentNote.Title;
    public final static SObjectField CONTENT_SOBJECTFIELD = Schema.ContentNote.Content;
    final static String TITLE_FIELD = TITLE_SOBJECTFIELD.getDescribe().getName().toLowerCase();
    final static String CONTENT_FIELD = CONTENT_SOBJECTFIELD.getDescribe().getName().toLowerCase();
    final static String EMPTY = '';

    Map<String, String> mapFieldValue;

    public ContentNoteMediator() {
        this.mapFieldValue = new Map<String, String>{ TITLE_FIELD => EMPTY,
                                                      CONTENT_FIELD => EMPTY};
    }

    public Id contentNoteId { get; set; }

    public Id linkedEntityId { get; set; }

    public String title {
        get { return mapFieldValue.get(TITLE_FIELD); }
        set { mapFieldValue.put(TITLE_FIELD, value); }
    }

    public String content {
        get { return mapFieldValue.get(CONTENT_FIELD); }
        set { mapFieldValue.put(CONTENT_FIELD, value); }
    }

    public Blob contentBlob {
        get { return Blob.valueOf(this.content); }
        set { this.content = value.toString(); }
    }

    public String getFieldValue(String fieldName) {
        return mapFieldValue.get(fieldName);
    }

    public Integer getHashCode() {
        return System.hashCode(this);
    }
}