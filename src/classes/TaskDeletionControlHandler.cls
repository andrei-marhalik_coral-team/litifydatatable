public class TaskDeletionControlHandler {
    public final static String ALLOW_DELETION_PERMISSION_SET_NAME = 'Allow_Task_Deletion';
    public final static String DELETION_UNPERMITTED_ERROR_MESSAGE = 'Sorry, but it seems like you do not have permission to delete this task.';
    
    public static void preventUnauthorizedDeletion(List<Task> tasks) {
        Boolean isDeletedPermitted = doesUserHasDeletePermission(UserInfo.getUserId());
        
        if(!isDeletedPermitted) {
            for(Task t : tasks) {
                t.addError(DELETION_UNPERMITTED_ERROR_MESSAGE);
            }
        }
    }
    
    private static Boolean doesUserHasDeletePermission(Id userId) {
        List<AggregateResult> c = [SELECT count(Id)
                                   FROM PermissionSetAssignment
                                   WHERE AssigneeId = :userId
                                   AND PermissionSet.Name = :ALLOW_DELETION_PERMISSION_SET_NAME];
        
        return ((Integer)c[0].get('expr0')) != 0;
    }
}