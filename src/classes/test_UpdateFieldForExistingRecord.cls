@isTest 
public class test_UpdateFieldForExistingRecord 
{
    
    @testSetup 
    static void testMethod1(){
        List<litify_pm__Matter__c> listMatters= new List<litify_pm__Matter__c>();
        List<Account> listClients= new List<Account>();
        
        Account acc = new Account();
        acc.litify_pm__Last_Name__c = 'Test_Last';
        insert acc;
        
        
        for(Integer i=0 ; i<200 ; i++)
        {
            litify_pm__Matter__c mat = new litify_pm__Matter__c();
            mat.litify_pm__Client__c = acc.Id;
            listMatters.add(mat);
        }  
        insert listMatters;
    	
    }   
    static testmethod void test() {  
        	 
        Test.startTest();
        
        UpdateFieldForExistingRecord obj = new UpdateFieldForExistingRecord();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
       
        /* System.assertEquals();*/
        
        list<litify_pm__Matter__c> newlistMatters = [SELECT Id, Name , Matter_Number_Accting__c FROM litify_pm__Matter__c];
        System.assertEquals(200, newlistMatters.size() );
        for(litify_pm__Matter__c lit : newlistMatters){
            System.assertEquals(lit.Matter_Number_Accting__c, lit.Name.right(6));
        }
        
    }
}