@isTest
public class TaskDeletionControlTest {
    private final static String USER_WITH_PERMISSION = 'userwith@permission.test.com';
    private final static String USER_WITHOUT_PERMISSION = 'userwithout@permission.test.com';
    
    @testSetup static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u_w_p = new User(Alias = 'uwperm', Email= USER_WITH_PERMISSION,
                              EmailEncodingKey='UTF-8', LastName='WithPermission', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', UserName=USER_WITH_PERMISSION);
        insert u_w_p;
        
        User u_wo_p = new User(Alias = 'uwoperm', Email= USER_WITHOUT_PERMISSION,
                               EmailEncodingKey='UTF-8', LastName ='WithoutPermission', LanguageLocaleKey='en_US',
                               LocaleSidKey='en_US', ProfileId = p.Id,
                               TimeZoneSidKey='America/Los_Angeles', UserName=USER_WITHOUT_PERMISSION);
        insert u_wo_p;
        
        PermissionSet ps = [SELECT Id FROM PermissionSet
                            WHERE Name = :TaskDeletionControlHandler.ALLOW_DELETION_PERMISSION_SET_NAME];
        
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = u_w_p.Id,
                                                                  PermissionSetId = ps.Id);
        insert psa;
    }
    
    static testMethod void testPermittedDeletion() {
        Test.startTest();
        
        User u_w_p = [SELECT Id FROM User WHERE UserName = :USER_WITH_PERMISSION LIMIT 1];
        
        Task t = new Task(OwnerId=u_w_p.Id);
        insert t;
        
        Id insertedTaskId = t.Id;
        
        System.runAs(u_w_p) {
	        delete t;    
        }
        
        List<Task> tasks = [SELECT Id FROM Task WHERE Id = :insertedTaskId];
        
        System.assertEquals(0, tasks.size());
        
        Test.stopTest();
    }
    
    static testMethod void testUnpermittedDeletion() {
        Test.startTest();
        
        User u_wo_p = [SELECT Id FROM User WHERE UserName = :USER_WITHOUT_PERMISSION LIMIT 1];
        
        Task t = new Task(OwnerId=u_wo_p.Id);
        insert t;

        System.runAs(u_wo_p) {
            try{
	        	delete t;
            } catch(Exception e) {
                System.assert(e.getMessage()
                              .contains(TaskDeletionControlHandler.DELETION_UNPERMITTED_ERROR_MESSAGE));
            }
        }
        
        Test.stopTest();
    }
}