global class UpdateFieldForExistingRecord implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String query = 'SELECT Id, Name , Matter_Number_Accting__c FROM litify_pm__Matter__c WHERE Matter_Number_Accting__c= null';
        return Database.getQueryLocator(query);   
    }
    global void execute(Database.BatchableContext BC, List<sObject> objects) 
    {
        List<litify_pm__Matter__c> matters = (List<litify_pm__Matter__c>)objects;
        for(litify_pm__Matter__c a : matters)   
        {
            a.Matter_Number_Accting__c = a.Name.right(6);   
        }
        update matters;   
    }
    global void finish(Database.BatchableContext BC) 
    {   
    }  
}