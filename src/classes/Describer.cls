@RestResource(urlMapping='/schema/describer/*')
global class Describer
{
    private static Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    
    @HttpPost
    global static void getObjectFields() {
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String jsonResponse = '';
        
        Map<String, List<String>> objectFieldsMap = getObjectFieldsMap();
        
        res.statusCode = 200;
        jsonResponse = Json.serialize(objectFieldsMap);
        res.responseBody = blob.valueOf(jsonResponse);
        return;
    }
    
    // find all sObjects available in the organization
    private static List<String> getObjNames() {
        List<String> entities = new List<String>(schemaMap.keySet());
        entities.sort();
        return entities;
    }
    
    // Find the fields for the objects
    private static Map<String, List<String>> getObjectFieldsMap() {
        List<String> objectNames = getObjNames();
        
        Map<String, List<String>> mapObjFields = new Map<String, List<String>>();
        for(String obj : objectNames) {
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get(obj).getDescribe().fields.getMap();
            
            List<String> objFields = new List<String>();
            for(Schema.SObjectField sfield : fieldMap.Values())
            {
                schema.describefieldresult dfield = sfield.getDescribe();
                objFields.add( dfield.getname() );
            }
            
            mapObjFields.put(obj, objFields);
        }
        
        return mapObjFields;
    }
}