@RestResource(urlMapping='/Test/*')
global class TestAPI {
	@HttpPost
    global static String convertStringToDate() {
        SObject obj = null;
        if (RestContext.request.requestBody != null) {
            JSONParser parser = JSON.createParser(RestContext.request.requestBody.toString());
            while (parser.nextToken() != null) {
                if(parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    if (parser.getText() == 'DateOfWedding') {
                        parser.nextToken();
                        if (parser.getCurrentToken() == JSONToken.VALUE_STRING) {
                            try {                                
                                litify_pm__Intake_Mapper__mdt intakeMapper = [SELECT Id,
                                          litify_pm__SObject_Name__r.developername,
                                          litify_pm__Field_Name__r.developername
                                        FROM litify_pm__Intake_Mapper__mdt
                                        WHERE MasterLabel = 'DateOfWedding'
                                        LIMIT 1];
                                String objectName = intakeMapper.litify_pm__SObject_Name__r.developername;
                                String fieldName = intakeMapper.litify_pm__Field_Name__r.developername;
                                SObjectType objectType  = Schema.getGlobalDescribe().get(objectName);
                                obj = objectType.newSObject();
                                obj.put('litify_pm__First_Name__c','Test');
                                obj.put('litify_pm__Last_Name__c', 'Test');
                                System.debug('Before put the date');
                                obj.put(fieldName+'__c', parser.getText());
                                System.debug('Before insert the object');
                                insert obj;                               
                            } catch(Exception e) {
                                return e.getMessage();
                            }
                        }
                    }
                }
            }
        }
        
        return obj.Id;
    }
}