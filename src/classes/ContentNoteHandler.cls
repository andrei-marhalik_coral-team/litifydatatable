public class ContentNoteHandler {
    static final String SHARE_TYPE = 'V';
    static final String VISIBILITY = 'AllUsers';
    static final Map<Schema.SObjectType, Map<Schema.SObjectField, Schema.SObjectField>>  mapObjectMapFieldField =
            new Map<SObjectType, Map<SObjectField, SObjectField>>{
                    Schema.Task.getSObjectType() =>
                            new Map<SObjectField, SObjectField>{ ContentNoteMediator.TITLE_SOBJECTFIELD => Schema.Task.Subject,
                                                                 ContentNoteMediator.CONTENT_SOBJECTFIELD => Schema.Task.Description },
                    Schema.EmailMessage.getSObjectType() =>
                            new Map<SObjectField, SObjectField>{ ContentNoteMediator.TITLE_SOBJECTFIELD => Schema.EmailMessage.Subject,
                                                                 ContentNoteMediator.CONTENT_SOBJECTFIELD => Schema.EmailMessage.HtmlBody }
            };

    public static void createContentNotes(List<ContentNoteMediator> contentNoteMediators) {
        Map<Integer, ContentNote> mapHashCodeContentNote = new Map<Integer, ContentNote>();

        for(ContentNoteMediator cNM : contentNoteMediators) {
            mapHashCodeContentNote.put( cNM.getHashCode(), createContentNote(cNM.title, cNM.contentBlob) );
        }

        insert mapHashCodeContentNote.values();

        List<ContentDocumentLink> linkContentDocuments = new List<ContentDocumentLink>();

        for(ContentNoteMediator cNM : contentNoteMediators) {
            linkContentDocuments.add( linkContentDocument( mapHashCodeContentNote.get( cNM.getHashCode() ).Id,
                                     cNM.linkedEntityId ) );
        }

        insert linkContentDocuments;
    }

    public static void updateLinkedObjects(List<ContentNoteMediator> contentNoteMediators) {
        List<SObject> sObjectsToUpdate = new List<SObject>();

        for(ContentNoteMediator cNM : contentNoteMediators) {
            SObject linkedEntity = cNM.linkedEntityId.getSObjectType().newSObject(cNM.linkedEntityId);

            Map<SObjectField, SObjectField> mapFieldField = mapObjectMapFieldField.get( cNM.linkedEntityId.getSObjectType() );

            for(SObjectField sOF : mapFieldField.keySet()) {
                linkedEntity.put(mapFieldField.get(sOF), cNM.getFieldValue( sOF.getDescribe().getName().toLowerCase() ));
            }

            sObjectsToUpdate.add(linkedEntity);
        }

        update sObjectsToUpdate;
    }

    public static void updateContentNotes(List<ContentNoteMediator> contentNoteMediators) {
        Set<Id> contentNoteIds = new Set<Id>();
        for(ContentNoteMediator cNM : contentNoteMediators) {
            contentNoteIds.add(cNM.contentNoteId);
        }

        Map<Id, ContentNote> mapIdContentNote = new Map<Id, ContentNote>([SELECT Id, Title, Content
                                                                          FROM ContentNote
                                                                          WHERE Id IN :contentNoteIds]);
        for(ContentNoteMediator cNM : contentNoteMediators) {
            if(mapIdContentNote.containsKey(cNM.contentNoteId)) {
                ContentNote cN = mapIdContentNote.get(cNM.contentNoteId);
                cN.Title = cNM.title;
                cN.Content = cNM.contentBlob;
            }
        }

        update mapIdContentNote.values();
    }

    private static SObject getSObjectById(Id xId) {
        Schema.SObjectType sobjectType = xId.getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();

        String fields = '';
        List<SObjectField> sObjectFields = mapObjectMapFieldField.get(sobjectType).values();
        for(SObjectField sOF : sObjectFields) {
            fields += sOF.getDescribe().getName() + ', ';
        }

        return Database.query('SELECT ' + fields + ' Id FROM ' + sobjectName + ' WHERE Id = :xId');
    }

    private static ContentNote createContentNote(String title, Blob content) {
        return new ContentNote(Title = title,
                               Content = content);
    }

    private static ContentDocumentLink linkContentDocument(Id contentNoteId, Id linkedEntityId) {
        return new ContentDocumentLink(ContentDocumentId = contentNoteId,
                                       LinkedEntityId = linkedEntityId,
                                       ShareType = SHARE_TYPE,
                                       Visibility = VISIBILITY);
    }
}