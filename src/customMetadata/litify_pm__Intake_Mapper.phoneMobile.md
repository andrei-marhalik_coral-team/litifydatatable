<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>phoneMobile</label>
    <protected>false</protected>
    <values>
        <field>litify_pm__Field_Name__c</field>
        <value xsi:type="xsd:string">litify_pm__Phone_Mobile__c</value>
    </values>
    <values>
        <field>litify_pm__SObject_Name__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
