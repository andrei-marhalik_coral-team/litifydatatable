<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>TestOut</fullName>
        <apiVersion>44.0</apiVersion>
        <endpointUrl>https://https//coralteam.io/</endpointUrl>
        <fields>Description__c</fields>
        <fields>Duration__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>paul.antonau@support.litify</integrationUser>
        <name>TestOut</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Upload Time Entry</fullName>
        <actions>
            <name>TestOut</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>TimeEntry__c.Duration__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
