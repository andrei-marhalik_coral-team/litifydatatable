trigger TestHistory on TestHistory__c (after update) {
    if(Trigger.isAfter && Trigger.isUpdate) {
        List<TestHistory__History>  thhs = new List<TestHistory__History>();
        
        for( Id thId : Trigger.newMap.keySet() ) {
            if( Trigger.oldMap.get( thId ).UntrackField__c != Trigger.newMap.get( thId ).UntrackField__c )
            {
				TestHistory__History thh = new TestHistory__History(ParentId = thId,
                                                                    Field = 'UntrackField__c');
                
                thhs.add(thh);
            }
        }
        
        insert thhs;
    }
}