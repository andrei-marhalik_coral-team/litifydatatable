trigger IntakeHistoryTracking on litify_pm__Intake__c (after insert, after update, before insert, before update) {
    if(Trigger.isUpdate)
    {
		List<ChangeTracker__c> tracker = new List<ChangeTracker__c>();
        
        litify_pm__Intake__c gplObject = new litify_pm__Intake__c(); // This takes all available fields from the required object. 
        Schema.SObjectType objType = gplObject.getSObjectType(); 
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.litify_pm__Intake__c.fields.getMap(); 
        
        for(litify_pm__Intake__c gpl : trigger.new)
        {
            litify_pm__Intake__c oldGPL = trigger.oldMap.get(gpl.Id);

            for (String str : mapFields.keyset()) 
            { 
                try 
                { 
                    if((gpl.get(str) != null)
                       && (oldGPL.get(str) != null)
                       && (oldGPL.get(str) != gpl.get(str)) )
                    { 
                         ChangeTracker__c ct = new ChangeTracker__c();
                         ct.Object__c = 'Intake';
                         ct.Field__c = str;
                         ct.OldValue__c = oldGPL.get(str).toString();
                         ct.NewValue__c = gpl.get(str).toString();
                        
                        tracker.add(ct);
                    } 
                } 
                catch (Exception e) 
                { 
                    System.Debug('sdfError: ' + e); 
                } 
            }
        }
        
        insert tracker;
    }
}