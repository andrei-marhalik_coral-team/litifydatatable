trigger TaskDeletionControl on Task (before delete) {
    if(Trigger.isBefore && Trigger.isDelete) {
        TaskDeletionControlHandler.preventUnauthorizedDeletion(Trigger.old);
    }
}