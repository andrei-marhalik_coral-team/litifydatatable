trigger PrivateActivity on Task (after insert) {
    Set <Id> tasksIDs = new Set<Id> ();
    List<Private_Activity__c> privateActivities = new List<Private_Activity__c>();
    List<Task> tasks = new List<Task>();

    tasks = [SELECT Id, Description, WhatId, Private__c  
            FROM Task
            WHERE Id IN :Trigger.newMap.keySet()
            AND Private__c = TRUE];

    if(tasks.size() > 0) {

        for(Task t: tasks) {
            Private_Activity__c pa = new Private_Activity__c();
            pa.Description__c = t.Description;
            pa.Matter__c = t.WhatId;
            privateActivities.add(pa);
        }

    }

    upsert(privateActivities);
    delete(tasks);
}