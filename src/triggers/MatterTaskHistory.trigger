trigger MatterTaskHistory on Task (after insert, after update) {
    // trigger for traking task activity
    List<TaskHistory__c> taskHistories = new List<TaskHistory__c>();


    if (Trigger.isInsert && Trigger.isAfter) {
         MatterTaskHistoryHandler.getInsertHistory(Trigger.new, taskHistories);
    } else if (Trigger.isUpdate && Trigger.isAfter) {
         MatterTaskHistoryHandler.getUpdateHistory(Trigger.newMap, Trigger.oldMap, taskHistories);
    }

    if(taskHistories.size() > 0) {
        insert taskHistories;
    }
}