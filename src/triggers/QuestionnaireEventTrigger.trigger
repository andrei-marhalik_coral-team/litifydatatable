trigger QuestionnaireEventTrigger on litify_pm__QuestionnaireAnswersSaved__e (after insert) {
    
	for (litify_pm__QuestionnaireAnswersSaved__e event : Trigger.New) {
        QuestionnaireEventLog__c log = new QuestionnaireEventLog__c();
        log.QuestionnaireOutputId__c = event.litify_pm__QuestionnaireOutputId__c;
        log.QuestionNodeId__c = event.litify_pm__QuestionNodeId__c;
        log.RelatedObjectId__c = event.litify_pm__RelatedObjectId__c;
        log.ReplayID__c = event.ReplayId;
        insert log;
    }
}