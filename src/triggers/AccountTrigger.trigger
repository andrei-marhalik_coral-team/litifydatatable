trigger AccountTrigger on Account (before insert) {
    for(Account a : Trigger.new) {
        try { 
            TaskHistory__c taskHistory = new TaskHistory__c(SessionId__c = UserInfo.getSessionId(),
                                                            TaskId__c = a.Id,
                                                            Type__c = 'Insert',
                                                            ChangedFields__c = 'Test Account Insertion');
            insert taskHistory;
        } catch(Exception e) {
            TaskHistory__c taskHistory = new TaskHistory__c(SessionId__c = UserInfo.getSessionId(),
                                                            Type__c = 'Insert',
                                                            ChangedFields__c = 'ERROR >>> ' + e.getMessage());
            insert taskHistory;
        }
    }
}