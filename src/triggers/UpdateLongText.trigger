trigger UpdateLongText on RegexObj__c (before insert) {
	for(RegexObj__c a : Trigger.New) {
        a.LongText__c = 'New description';
    }
}